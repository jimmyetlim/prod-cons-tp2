package ca.qc.claurendeau.storage;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestElement {

	Element testElement;
	int data;
	int dataMod;
	
	@Before
	public void Setup() {
		data = 10;
		dataMod = 15;
		testElement = new Element(data);	
	}
	
	@Test
	public void testGetELement() {
		assertTrue(testElement.getData()==data);
	}
	
	@Test
	public void testSetData() {
		testElement.setData(dataMod);
		assertEquals(testElement.getData(),dataMod);
		assertNotEquals(testElement.getData(),data);
	}

}
