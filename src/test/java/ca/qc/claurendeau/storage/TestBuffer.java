package ca.qc.claurendeau.storage;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestBuffer {

	Buffer buff;
	int capacity;
	Element test;
	int data;
	String resultString;
	
	@Before
	public void Setup() {
		capacity =1;
		buff=new Buffer(capacity);
		data = 10;
		test=new Element (data);
		buff.addElement(test);
		resultString = "["+data+"]";
		
	}
	
	@Test
	public void testCapacity() {
		assertEquals(buff.capacity(),capacity);
	}
	@Test
	(expected=Exception.class)
	public void testAddElement() {
		buff.addElement(test);
	}
	
	@Test
	public void testIsFull() {
		assertTrue(buff.isFull());
	}
	
	@Test
	public void testGetCurrentLoad() {
		assertEquals(1, buff.getCurrentLoad());;
	}
	
	@Test
	public void testRemove() {
		assertEquals(test, buff.removeElement());
	}
	
	@Test
	(expected=Exception.class)
	public void testRemoveEmpty() {
		assertEquals(test, buff.removeElement());
		assertEquals(test, buff.removeElement());
	}
	
	@Test
	public void testIsEmpty() {
		assertFalse(buff.isEmpty());
		assertEquals(test, buff.removeElement());
		assertTrue(buff.isEmpty());
	}
	
	@Test
	public void testToString() {
		assertTrue(buff.toString().equals(resultString));
	}

}
