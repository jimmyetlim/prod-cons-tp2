package ca.qc.claurendeau.storage;

import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

import ca.qc.claurendeau.exception.BufferEmptyException;
import ca.qc.claurendeau.exception.BufferFullException;

public class Buffer
{
    private Queue<Element> dataTable;
    private int capacity;

    public Buffer(int capacity)
    {
        this.dataTable = new ArrayBlockingQueue<>(capacity);
        this.capacity=capacity;
    }

    // returns the content of the buffer in form of a string
    public String toString()
    {
        String string = dataTable.toString();
        return string;
    }

    // returns the capacity of the buffer
    public int capacity()
    {
        return capacity;
    }

    // returns the number of elements currently in the buffer
    public int getCurrentLoad()
    {
        return dataTable.size();
    }

    // returns true if buffer is empty, false otherwise
    public boolean isEmpty()
    {
        return dataTable.size()==0;
    }

    // returns true if buffer is full, false otherwise
    public boolean isFull()
    {
        return capacity == dataTable.size();
    }

    // adds an element to the buffer
    // Throws an exception if the buffer is full
    public synchronized void addElement(Element element) throws BufferFullException
    {
        if(isFull()) 
        {
        	throw new BufferFullException("Buffer Full");
        }else 
        {
        	dataTable.add( element );
        }
    }
    
    // removes an element and returns it
    // Throws an exception if the buffer is empty
    public synchronized Element removeElement()  throws BufferEmptyException
    {
        if(isEmpty()) {
        	throw new BufferEmptyException("Buffer Full");
        }else {
        	return dataTable.poll();
        }
    }
}
