package ca.qc.claurendeau.exception;

@SuppressWarnings("serial")
public class BufferEmptyException extends RuntimeException {
	 public BufferEmptyException() {
         super();
     }
     public BufferEmptyException(String s) {
         super(s);
     }
     public BufferEmptyException(String s, Throwable throwable) {
         super(s, throwable);
     }
     public BufferEmptyException(Throwable throwable) {
         super(throwable);
     }
}
