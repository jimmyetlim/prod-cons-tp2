package ca.qc.claurendeau.exception;

@SuppressWarnings("serial")
public class BufferFullException extends RuntimeException {
	 public BufferFullException() {
         super();
     }
     public BufferFullException(String s) {
         super(s);
     }
     public BufferFullException(String s, Throwable throwable) {
         super(s, throwable);
     }
     public BufferFullException(Throwable throwable) {
         super(throwable);
     }
}
